﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Polygons
{
    public partial class Form1 : Form
    {
        private List<Point> poly = new List<Point>();

        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            poly.Add(new Point(e.X, e.Y));
            using (Graphics g = pictureBox1.CreateGraphics())
            {
                g.FillEllipse(Brushes.Red, new Rectangle(e.X - 5, e.Y - 5, 10, 10));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (poly.Count > 2)
            {
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                e.Graphics.Clear(Color.White);

                using (SolidBrush br = new SolidBrush(Color.FromArgb(100, Color.Yellow)))
                {
                    e.Graphics.FillPolygon(br, poly.ToArray());
                }
                e.Graphics.DrawPolygon(Pens.DarkBlue, poly.ToArray());

                foreach (Point p in poly)
                {
                    e.Graphics.FillEllipse(Brushes.Red,
                                           new Rectangle(p.X - 5, p.Y - 5, 10, 10));
                }

                poly.Clear();
            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {

        }
    }
}
